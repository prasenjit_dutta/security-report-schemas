#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SAST_SCHEMA="$PROJECT_DIRECTORY/dist/sast-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_sast_contains_common_definitions() {
  verify_schema_contains_selector "$SAST_SCHEMA" ".title"
  verify_schema_contains_selector "$SAST_SCHEMA" ".description"
  verify_schema_contains_selector "$SAST_SCHEMA" ".self.version"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.scan.properties.end_time"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.scan.properties.messages"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.scan.properties.scanner"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.scan.properties.start_time"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.scan.properties.status"
  verify_schema_contains_selector "$SAST_SCHEMA" '.properties.scan.properties.type.enum == ["sast"]'
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.schema"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.version"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.category"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.name"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.message"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.description"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.cve"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.severity"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.confidence"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.solution"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.scanner"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.identifiers"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.links"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.details"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.detail_type"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.text_value"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.named_field"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.named_list"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.list"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.table"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.text"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.url"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.code"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.value"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.diff"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.markdown"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.commit"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.file_location"
  verify_schema_contains_selector "$SAST_SCHEMA" ".definitions.module_location"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.remediations.items.properties.fixes"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.remediations.items.properties.summary"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.remediations.items.properties.diff"
}

test_sast_extensions() {
  verify_schema_contains_selector "$SAST_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.start_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.end_line"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.class"
  verify_schema_contains_selector "$SAST_SCHEMA" ".properties.vulnerabilities.items.properties.raw_source_code_extract"
}
