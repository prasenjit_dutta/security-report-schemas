import dast from './dast/index'
import sast from './sast/index'
import dependency_scanning from './dependency_scanning/index'
import secret_detection from './secret_detection/index'
import container_scanning from './container_scanning/index'
import coverage_fuzzing from './coverage_fuzzing/index'

export default {
  dast,
  sast,
  dependency_scanning,
  secret_detection,
  container_scanning,
  coverage_fuzzing
}
